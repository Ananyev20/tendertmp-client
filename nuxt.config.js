module.exports = {
    /*
    ** Headers of the page
    */
    head: {
        title: 'tendertmp-client',
        meta: [
        { charset: 'utf-8' },
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
        { hid: 'description', name: 'description', content: 'Client for API of redacting table TempTender' }
        ],
        link: [
        { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
        ]
    },
    /*
    ** Customize the progress bar color
    */
    loading: '~/components/ProgressBar.vue',
    /*
    ** Build configuration
    */
    build: {
        /*
        ** Run ESLint on save
        */
        extend (config, { isDev, isClient }) {
        if (isDev && isClient) {
            config.module.rules.push({
            enforce: 'pre',
            test: /\.(js|vue)$/,
            loader: 'eslint-loader',
            exclude: /(node_modules)/
            })
        }
        }
    },

    modules: [
        'nuxt-buefy',
        '@nuxtjs/axios',
    ],
    buildModules: [
        '@nuxtjs/dotenv'
    ],

    plugins: ['~/plugins/vue-json-tree-view.js'],

    axios: {
        headers: {
            common: {
                'Authorization': '0x58b1a38e'
            }
        }
    }
}

