import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = () =>
    new Vuex.Store({
        state: {
            currencies: [],
            placements: [],
            tenders:    [],
            error: 	    null,
        },

        mutations: {
            setCurrencies(state, currencies){
                state.currencies = currencies;
            },

            setPlacements(state, placements) {
                state.placements = placements;
                placements.forEach(place => {
                    place.count = parseInt(place.count);
                    place.domId = place.domId || 'other';
                    place.statusPlacement = parseInt(place.statusPlacement) || 0;
                });
            },

            setPlacementStatus({ placements }, { placementId, status }) {
                let place = placements.filter( place => place.placementId == placementId)[0];

                if(place)
                    place.statusPlacement = status;
            },

            setTenders({ placements }, { placementId, tenders }){
                let place = placements.filter( place => place.placementId == placementId)[0];

                if(place)
                    place.count = tenders.length;
            },
            
            setError(state, error){
                state.error = error;
                console.error('Store Error:', error);
            },

            setAxiosError(state, axiosErr){
                let error = '';
                if(axiosErr.response){
                    error = 'Ошибка. Сервер ответил статусом: ' + axiosErr.response.status;

                    if(axiosErr.response.data.error)
                        error = axiosErr.response.data.error;
                }
                else
                    error = `Error. Axios config:  ${JSON.stringify(axiosErr.config, null, 4)}`;

                state.error = error;
                console.error('Axios Error:', error);
            }
        },
        actions: {

            async loadCurrencies({ commit }) {
                let currencies = [];

                try{
                    currencies = await this.$axios.$get('currency');
                } catch(err){
                    commit('setAxiosError', err);
                    return [];
                }

                commit('setCurrencies', currencies);
                return currencies;
            },

            async loadPlacements({ commit }) {
                let placements = [];

                try{
                    placements = await this.$axios.$get('placements');
                } catch(err){
                    commit('setAxiosError', err);
                }

                commit('setPlacements', placements);
                return placements;
            },

            async setPlacementStatus({ commit }, { placementId, status }) {
                let res = {};

                try{
                    res = await this.$axios.$post(`placement/${placementId}`, { status });
                } catch(err){
                    commit('setAxiosError', err);
                }

                commit('setPlacementStatus', { placementId, status });
                return res;
            },

            async loadTenders({ commit }, placementId) {
                let tenders = [];

                try{
                    tenders = await this.$axios.$get(`placement/${placementId}`);
                } catch(err){
                    if(err.response.status !== 501 
                        || err.response.data.success !== false)
                            commit('setAxiosError', err);
                }

                commit('setTenders', { placementId, tenders });
                return tenders;
            },

            async loadRedactTender({ commit }, tenderId) {
                let tender = {};

                try{
                    tender = await this.$axios.$get(`tender/${tenderId}`);
                } catch(err){
                    if(err.response.status != 501 
                        || err.response.data.success !== false)
                            commit('setAxiosError', err);
                }

                return tender;
            },

            async updateTender({ commit }, tender) {
                let res = {};

                try{
                    res = await this.$axios.$post(`tender/${tender.notificationNumber}`, tender);
                } catch(err){
                    commit('setAxiosError', err);
                }

                return res;
            },

            async deleteTender({ commit }, tenderId) {
                let res = {};

                try{
                    res = await this.$axios.$delete(`tender/${tenderId}`);
                } catch(err){
                    commit('setAxiosError', err);
                }

                return res;
            },
        },
        strict: true
    })

export default store